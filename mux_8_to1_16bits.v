// a mux_8to1_16bits

module mux_8to1_16bits(
    input  [15:0] A0, A1, A2, A3, A4, A5, A6, A7,
    input  [2:0] sel,
    output reg [15:0] Y
);

    always @(*) begin
        case(sel)
           3'b000:Y=A0;
           3'b001:Y=A1;
           3'b010:Y=A2;
           3'b011:Y=A3;

           3'b100:Y=A4;
           3'b101:Y=A5;
           3'b110:Y=A6;
           3'b111:Y=A7;
       endcase
    end

endmodule
