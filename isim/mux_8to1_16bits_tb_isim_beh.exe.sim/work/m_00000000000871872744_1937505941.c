/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "";
static const char *ng1 = "ns %h %h %h %h %h %h %h %h %h %h";
static const char *ng2 = "D:/ISE_design/mux_8_to1_16bits/mux_8to1_16bits.tb.v";
static int ng3[] = {55530, 0};
static int ng4[] = {55531, 0};
static int ng5[] = {55532, 0};
static int ng6[] = {55533, 0};
static int ng7[] = {55534, 0};
static int ng8[] = {55535, 0};
static int ng9[] = {55536, 0};
static int ng10[] = {55537, 0};
static unsigned int ng11[] = {0U, 0U};
static unsigned int ng12[] = {1U, 0U};
static unsigned int ng13[] = {2U, 0U};
static unsigned int ng14[] = {3U, 0U};
static unsigned int ng15[] = {4U, 0U};
static unsigned int ng16[] = {5U, 0U};
static unsigned int ng17[] = {6U, 0U};
static unsigned int ng18[] = {7U, 0U};

void Monitor_36_4(char *);
void Monitor_36_4(char *);


static void Monitor_36_4_Func(char *t0)
{
    char t2[8];
    double t1;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    char *t23;
    char *t24;
    char *t25;
    char *t26;
    char *t27;
    char *t28;
    char *t29;
    char *t30;
    char *t31;

LAB0:    t1 = xsi_vlog_realtime(1000.0000000000000, 10.000000000000000);
    *((double *)t2) = t1;
    xsi_vlogfile_write(0, 0, 3, ng0, 2, t0, (char)114, t2, 64);
    t3 = (t0 + 1448);
    t4 = (t3 + 56U);
    t5 = *((char **)t4);
    t6 = (t0 + 1608);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t9 = (t0 + 1768);
    t10 = (t9 + 56U);
    t11 = *((char **)t10);
    t12 = (t0 + 1928);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t15 = (t0 + 2088);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t0 + 2248);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t0 + 2408);
    t22 = (t21 + 56U);
    t23 = *((char **)t22);
    t24 = (t0 + 2568);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    t27 = (t0 + 2728);
    t28 = (t27 + 56U);
    t29 = *((char **)t28);
    t30 = (t0 + 1048U);
    t31 = *((char **)t30);
    xsi_vlogfile_write(1, 0, 3, ng1, 11, t0, (char)118, t5, 16, (char)118, t8, 16, (char)118, t11, 16, (char)118, t14, 16, (char)118, t17, 16, (char)118, t20, 16, (char)118, t23, 16, (char)118, t26, 16, (char)118, t29, 3, (char)118, t31, 16);

LAB1:    return;
}

static void Initial_12_0(char *t0)
{
    char *t1;
    char *t2;

LAB0:    xsi_set_current_line(12, ng2);

LAB2:    xsi_set_current_line(13, ng2);
    t1 = ((char*)((ng3)));
    t2 = (t0 + 1448);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(14, ng2);
    t1 = ((char*)((ng4)));
    t2 = (t0 + 1608);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(15, ng2);
    t1 = ((char*)((ng5)));
    t2 = (t0 + 1768);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(16, ng2);
    t1 = ((char*)((ng6)));
    t2 = (t0 + 1928);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(17, ng2);
    t1 = ((char*)((ng7)));
    t2 = (t0 + 2088);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(18, ng2);
    t1 = ((char*)((ng8)));
    t2 = (t0 + 2248);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(19, ng2);
    t1 = ((char*)((ng9)));
    t2 = (t0 + 2408);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);
    xsi_set_current_line(20, ng2);
    t1 = ((char*)((ng10)));
    t2 = (t0 + 2568);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 16);

LAB1:    return;
}

static void Initial_22_1(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;

LAB0:    t1 = (t0 + 3896U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(22, ng2);

LAB4:    xsi_set_current_line(23, ng2);
    t2 = ((char*)((ng11)));
    t3 = (t0 + 2728);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 3);
    xsi_set_current_line(24, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 50000LL);
    *((char **)t1) = &&LAB5;

LAB1:    return;
LAB5:    xsi_set_current_line(24, ng2);
    t3 = ((char*)((ng12)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(25, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 50000LL);
    *((char **)t1) = &&LAB6;
    goto LAB1;

LAB6:    xsi_set_current_line(25, ng2);
    t3 = ((char*)((ng13)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(26, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 100000LL);
    *((char **)t1) = &&LAB7;
    goto LAB1;

LAB7:    xsi_set_current_line(26, ng2);
    t3 = ((char*)((ng14)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(27, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 50000LL);
    *((char **)t1) = &&LAB8;
    goto LAB1;

LAB8:    xsi_set_current_line(27, ng2);
    t3 = ((char*)((ng15)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(28, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 50000LL);
    *((char **)t1) = &&LAB9;
    goto LAB1;

LAB9:    xsi_set_current_line(28, ng2);
    t3 = ((char*)((ng16)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(29, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 100000LL);
    *((char **)t1) = &&LAB10;
    goto LAB1;

LAB10:    xsi_set_current_line(29, ng2);
    t3 = ((char*)((ng17)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(30, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 50000LL);
    *((char **)t1) = &&LAB11;
    goto LAB1;

LAB11:    xsi_set_current_line(30, ng2);
    t3 = ((char*)((ng18)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    xsi_set_current_line(31, ng2);
    t2 = (t0 + 3704);
    xsi_process_wait(t2, 100000LL);
    *((char **)t1) = &&LAB12;
    goto LAB1;

LAB12:    xsi_set_current_line(31, ng2);
    t3 = ((char*)((ng11)));
    t4 = (t0 + 2728);
    xsi_vlogvar_assign_value(t4, t3, 0, 0, 3);
    goto LAB1;

}

static void Initial_34_2(char *t0)
{
    char *t1;
    char *t2;

LAB0:    t1 = (t0 + 4144U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(34, ng2);
    t2 = (t0 + 3952);
    xsi_process_wait(t2, 1000000LL);
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(34, ng2);
    xsi_vlog_finish(1);
    goto LAB1;

}

static void Initial_35_3(char *t0)
{

LAB0:    xsi_set_current_line(36, ng2);
    Monitor_36_4(t0);

LAB1:    return;
}

void Monitor_36_4(char *t0)
{
    char *t1;
    char *t2;

LAB0:    t1 = (t0 + 4448);
    t2 = (t0 + 4960);
    xsi_vlogfile_monitor((void *)Monitor_36_4_Func, t1, t2);

LAB1:    return;
}


extern void work_m_00000000000871872744_1937505941_init()
{
	static char *pe[] = {(void *)Initial_12_0,(void *)Initial_22_1,(void *)Initial_34_2,(void *)Initial_35_3,(void *)Monitor_36_4};
	xsi_register_didat("work_m_00000000000871872744_1937505941", "isim/mux_8to1_16bits_tb_isim_beh.exe.sim/work/m_00000000000871872744_1937505941.didat");
	xsi_register_executes(pe);
}
